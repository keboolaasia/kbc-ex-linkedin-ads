import pip
pip.main(['install', 'beautifulsoup4', 'parsedatetime', 'PySocks', 'html5lib', 'lxml', 'requests[socks]'])
import requests
import os
import pandas as pd
import json
import sys
import traceback
from urllib.parse import urlencode
import datetime
from pytz import timezone
from keboola import docker
import subprocess
import socks
import parsedatetime
from bs4 import BeautifulSoup

"""

example config:

{
    "accountId": 504337514,
    "campaignIds": [123356095, 123356075, 123734325],
    "username": "gmail.com",
    "#password": "supersecret",
    "startDate": "7 days ago",
    "endDate": "today"
}
"""
REQUIRED_PARAMS = ['accountId', 'campaignIds', 'username',
                   '#password', 'startDate', 'endDate', 'period']

class ExtractorProxyError(Exception):
    """Used when current ip is blacklisted
    """
    pass
def fetch_list_of_proxies():
    """fetch and parse list of free proxies

    Returns:
        a list of proxies in the format socks5://{ip_address}:{port}
        ready to be used in requests
    """
    proxy_master_link = 'https://www.socks-proxy.net/'
    print("Fetching a list of proxies from", proxy_master_link)

    r = requests.get(proxy_master_link)

    try:
        r.raise_for_status()
    except Exception as e:
        print(e.response.text)
        raise

    df = pd.read_html(r.content)[0]
    ok_proxies = df[(df.Https == 'Yes') & (df.Version=='Socks5')].copy()
    ok_proxies.loc[:, "Port"] = ok_proxies.Port.astype(int)
    ok_proxies['conn_string'] = ok_proxies.apply(
        lambda row: 'socks5://{ip}:{port}'.format(
            ip=row['IP Address'],
            port=row['Port']),
        axis=1)
    return list(ok_proxies['conn_string'].unique())


def parse_config(datadir):
    cfg = docker.Config(datadir)
    params = cfg.get_parameters()
    for p in REQUIRED_PARAMS:
        if p not in params:
            raise ValueError("config parameter '{}' must be defined".format(p))
    return params

def _get_csrf(session):
    url = 'https://www.linkedin.com/'
    resp = session.get(url)
    soup = BeautifulSoup(resp.content, "html.parser")
    csrf_token = soup.find('input', id='loginCsrfParam-login')['value']
    return session, csrf_token


def login(session, username, password):
    print("Logging in")
    session, csrf = _get_csrf(session)
    payload = {'session_key': username, 'session_password': password,
               'loginCsrfParam': csrf, 'isJsEnabled': 'false'}
    r = session.post("https://www.linkedin.com/uas/login-submit", timeout=5, data=payload)
    r.raise_for_status()
    print("Headers:", session.headers)
    # print(r.text)
    # print(requests.utils.dict_from_cookiejar(r.cookies))
    # cookies = r.cookies
    # r = r.json()
    return session, csrf

def download_file(session, url, outpath):
    print("Downloading report...")
    with open(outpath, 'wb') as handle:
        # r = requests.get(url,  cookies=cookies, stream=True)
        r = session.get(url, stream=True)
        print("Headers:", session.headers)
        try:
            r.raise_for_status()
        except:
            print(r.content)
            raise
        print("status code:", r.status_code)
        for block in r.iter_content(1024):
            handle.write(block)
    return outpath

def build_url(account_id, campaign_ids, start_date, end_date, period='day'):
    """
    Args:
        account_id:
        campaign_ds: a list of camapign_ids
        start_date (str): %Y-%m-%dT%H:%M:%s.000Z (for example 2017-09-14T00:00:00.000Z)
        end_date (str): %Y-%m-%dT%H:%M:%s.000Z (for example 2017-09-14T00:00:00.000Z)
    """
    campaign_ids_full = ''.join('campaignIds={}&'.format(i) for i in campaign_ids)
    start_date_encoded = urlencode({'startDate': start_date})
    end_date_encoded = urlencode({'endDate': end_date})
    print("Building url")

    url = ('https://www.linkedin.com/ad/accounts/{account_id}/campaigns/report.csv?'
            '{campaign_ids}'
            '{start_date}&'
            '{end_date}&'
            'period=day').format(
                account_id=account_id,
                campaign_ids=campaign_ids_full,
                start_date=start_date_encoded,
                end_date=end_date_encoded)
    return url


def parsedt(dtstr):
    cal = parsedatetime.Calendar()
    date_struct, status = cal.parseDT(datetimeString=dtstr,
                                      tzinfo=timezone("UTC"))

    if status == 0:
        raise ValueError("Couldn't convert '{}' to valid datetime".format(dtstr))
    dt = str(date_struct.date())
    print("Converting '{}' to {}".format(dtstr, dt))
    return dt


def clean_report(pathin, pathout):
    print("Cleaning report")
    rep = pd.read_csv(pathin, skiprows=5, sep='\t', encoding='utf-16')
    rep.to_csv(pathout, encoding='utf-8', index=False)
    return pathout


def write_manifest(csvpath):
    print("Creating manifest for", csvpath)
    manifest_path = csvpath + '.manifest'
    config = {
        "destination": 'in.c-ex-linkedin-ads.ads_report'
    }
    with open(manifest_path, 'w') as fh:
        json.dump(config, fh)
    return manifest_path


def download_and_process_report(
        username,
        password,
        start_date,
        end_date,
        account_id,
        campaign_ids,
        period,
        proxies=None):

    print("Downloading report through proxy", proxies)
    with requests.Session() as session:
        session.proxies = proxies
        session, csrf = login(session,username, password)
        start_date = parsedt(start_date) + 'T00:00:00.000Z'
        end_date = parsedt(end_date) + 'T23:59:59.999Z'
        url = build_url(
            account_id=account_id,
            campaign_ids=campaign_ids,
            start_date=start_date,
            end_date=end_date,
            period=period)
        tmp_csv = download_file(
            session,
            url,
            "/tmp/test_campaign.csv")
    print("Report downloaded to {}".format(tmp_csv))
    print("Cleaning report")
    try:
        final_report_path = clean_report(tmp_csv, '/data/out/tables/report.csv')
    except UnicodeError:
        raise ExtractorProxyError(
            "Connected to proxy {}, but it's probably blacklisted".format(proxies))
    print("Final report saved to", final_report_path)
    write_manifest(final_report_path)


def main(datadir):
    params = parse_config(datadir)
    all_proxies = fetch_list_of_proxies()
    for proxy in all_proxies:
        proxies = {
            'http': proxy,
            'https': proxy
        }
        try:
            download_and_process_report(
                username=params['username'],
                password=params['#password'],
                start_date=params['startDate'],
                end_date=params['endDate'],
                account_id=params['accountId'],
                campaign_ids=params['campaignIds'],
                period=params['period'],
                proxies=proxies)
        except (ExtractorProxyError,
                requests.exceptions.ConnectionError,
                requests.exceptions.ProxyError,
                requests.exceptions.ReadTimeout) as e:
            print(e)
            print("Trying next one")
        else:
            print("Report downloaded using proxy {}".format(proxy))
            break
    else:
        raise ExtractorProxyError(
            "None of the list of {} proxies worked, the report couldn't"
            "be downloaded!".format(len(all_proxies)))



if __name__ == '__main__':
    try:
        main(os.getenv("KBC_DATADIR"))
    except ValueError as e:
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)
    except ExtractorProxyError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        traceback.print_exc(file=sys.stdout)
        sys.exit(2)
