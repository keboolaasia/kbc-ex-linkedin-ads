from main import build_url, parsedt

def test_building_url():
    expected = 'https://www.linkedin.com/ad/accounts/504337514/campaigns/report.csv?campaignIds=123356095&campaignIds=123356075&campaignIds=123734325&startDate=2017-09-14T00%3A00%3A00.000Z&endDate=2017-10-13T23%3A59%3A59.999Z&period=day'
    url = build_url(account_id=504337514,
                    campaign_ids=[123356095, 123356075, 123734325],
                    start_date='2017-09-14T00:00:00.000Z',
                    end_date='2017-10-13T23:59:59.999Z',
                    period='day')

    assert url == expected

def test_parsing_datetime():
    parsedt("")

